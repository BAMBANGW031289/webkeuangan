<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi', function (Blueprint $table) {
            $table->increments('id_transaksi');
            $table->string('jenis_trans');
            $table->string('nama');
            $table->decimal('nominal', 18)->nullable($value = true);
            $table->string('deskripsi');
            $table->decimal('debit', 18)->nullable($value = true);
            $table->decimal('kredit', 18)->nullable($value = true);
            $table->decimal('total', 18)->nullable($value = true);
            $table->string('updated_at');
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}
