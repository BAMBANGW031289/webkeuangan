<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = "transaksi";

     protected $fillable = ['jenis_trans', 'nama', 'nominal', 'deskripsi', 'total', 'debit', 'kredit'];
}
