<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Kategori;

use DB;

class KategoriController extends Controller{

public function index()
{
  $kategori = Kategori::all();
  return view('kategori.kategori', ['kategori' => $kategori]);
}

public function tambah()
{
  $kategori = Kategori::all();
  return view('kategori.kategori_tambah', ['kategori_tambah' => $kategori]);
}

public function store(Request $request)
{
  $this->validate($request,[
    'jenis' => 'required',
    'nama' => 'required',
    'deskripsi' => 'required',
  ]);

  Kategori::create([
    'jenis' => $request->jenis,
    'nama' => $request->nama,
    'deskripsi' => $request->deskripsi,
  ]);

  return redirect('/kategori')->with(['success' => 'Data Berhasil di Insert gaesss...']);
}

// public function edit($id)
// {
//   //$kategori = Kategori::find($id); // Query ke database untuk mengambil data dengan id yang diterima
//   $kategori = DB::table('kategori')->where('id',$id)->first();
//          return view('kategori_edit', ['kategori' => $kategori]);
// }

public function edit($id_kategori)
{
    $kategori = Kategori::where('id_kategori', $id_kategori)->firstOrFail();
    return view('kategori.kategori_edit', ['kategori' => $kategori]);
}

 public function update(Request $request, $id)
{
    $this->validate($request,[
      'jenis' => 'required',
    'nama' => 'required',
    'deskripsi' => 'required',
    ]);

        $form_data = array(
            'jenis' => $request->jenis,
            'nama' => $request->nama,
            'deskripsi' => $request->deskripsi,
        );
  
        Kategori::where('id_kategori', $id)->update($form_data);

    return redirect('/kategori')->with(['success' => 'Data Berhasil di update gaesss...']);

}

 public function delete($id)
{
    $kategori = Kategori::where('id_kategori', $id);
    $kategori->delete();
    
    return redirect('/kategori')->with(['danger' => 'Data Berhasil di Hapus gaesss...']);
}

}
