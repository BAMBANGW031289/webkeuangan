<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use DB;

class UserController extends Controller{

public function index()
{
  //$kategories = Kategori::all();
  $user = User::all();
  //$kategori =DB::table('kategori')->select('id_kategori','nama')->get();
  return view('user.user', ['user' => $user]);
//return view('user.user',compact('kategori','user'));
}



public function tambah()
{
 $kategori =DB::table('kategori')->select('id_kategori','nama')->get();
$user= DB::table('users')->select('*')->get();
return view('user.user_tambah',compact('kategori','user'));
}



public function store(Request $request)
{
  
// $gmbr = $request->image;
// $namaFile = time().rand(100,999).".".$gmbr->getClientOriginalExtension();

  $dtUpload = new User;
  $dtUpload->name = $request->name;
  $dtUpload->email = $request->email;
  $dtUpload->password = bcrypt($request->password);

  // $dtUpload->image = $namaFile;

  // $gmbr->move(public_path().'/img', $namaFile);
  $dtUpload->save();

   return redirect('/user')->with(['success' => 'Data Berhasil di Insert gaesss...']);
//  dd($request->all());
}

public function create(Request $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        return redirect('/user')->with(['success' => 'Data Berhasil di Insert gaesss...']);
    }




public function edit($id)
{
  $user = User::where('id', $id)->firstOrFail();
  // $kategori = Kategori::where('id_kategori', $id_kategori)->firstOrFail();
     $kategori =DB::table('kategori')->select('id_kategori','nama')->get();

return view('user.user_edit',compact('kategori','user'));
}

public function view($id)
{
    $user = User::find($id);
    return view('view', ['user' => $user]);
}


 public function delete($id)
{
    $user = User::where('id_user', $id);
    $user->delete();
    return redirect('/user');
}

 public function update(Request $request, $id)
    {
        $image_name = $request->hidden_image;
        $image = $request->file('image');
        if($image != '')
        {
            $namaFile = rand(100,999) . '.' . $image->getClientOriginalExtension();
            $image->move(public_path().'/img', $namaFile);
        }

        $form_data = array(
            'kode_user'       =>   $request->kode_user,
            'judul_user'      =>   $request->judul_user,
            'penulis_user'    =>   $request->penulis_user,
            'penerbit_user'   =>   $request->penerbit_user,
            'tahun_penerbit'  =>   $request->tahun_penerbit,
            'stok'            =>   $request->stok,
            'kategori'        =>   $request->kategori,
            'image'           =>   $image_name
        );
  
        User::where('id_user', $id)->update($form_data);

      return redirect('/user')->with(['success' => 'Data Berhasil di Update gaesss...']);
    }


}

