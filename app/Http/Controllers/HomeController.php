<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaksi;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        error_reporting(0);
         $transaksi = Transaksi::select('total')
                    ->orderBy('created_at', 'desc')
                    ->limit(1)
                    ->first();
          $transaksi_dk = Transaksi::all();

        return view('home',compact('transaksi', 'transaksi_dk'));
    }
}
