<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Transaksi;
use App\Anggota;
use Illuminate\Support\Facades\DB;

class TransaksiController extends Controller{

public function index()
{
   error_reporting(0);
  $ttotal = Transaksi::select('total')
                    ->orderBy('created_at', 'desc')
                    ->limit(1)
                    ->first();
  $transaksi = Transaksi::all();
  $kategori =DB::table('kategori')->select('id_kategori','nama')
                                  ->get();
  $trans_pemasukan = Transaksi::where('jenis_trans', 'pemasukan')
                               // ->where('nama', 'mushaf')
                                 ->orderBy('created_at', 'desc')
                                   ->limit(1)
                                     ->first();
                                     
  $trans_pengeluaran = Transaksi::where('jenis_trans', 'pengeluaran')
                                //->where('jenis_quran', 'mushaf')
                                 ->orderBy('created_at', 'desc')
                                   ->limit(1)
                                     ->first();                                     


return view('transaksi.transaksi',compact('kategori','transaksi','trans_pemasukan','trans_pengeluaran','ttotal'));
}


public function store(Request $request)
{
  $ttotal = Transaksi::select('total')
                    ->orderBy('created_at', 'desc')
                    ->limit(1)
                    ->first();
  $dtStore = new Transaksi;
  $dtStore->total =0;
  $dtStore->jenis_trans = $request->jenis_trans;
  $dtStore->nama = $request->nama;
  $dtStore->nominal = $request->nominal;
   if ($request->jenis_trans == 'pemasukan') {
            $dtStore->total = $ttotal->total + $request->nominal;
            $dtStore->debit = $request->nominal;
        } else {
            $dtStore->total = $ttotal->total - $request->nominal;
            $dtStore->kredit = $request->nominal;
        }    
  $dtStore->deskripsi = $request->deskripsi;
  $dtStore->save();

   return redirect('/transaksi')->with(['success' => 'Data Berhasil di Insert gaesss...']);
//  dd($request->all());
}

public function edit($id)
{
  $Transaksi = Transaksi::where('id_transaksi', $id)->firstOrFail();
  // $kategori = Kategori::where('id_kategori', $id_kategori)->firstOrFail();
     $kategori =DB::table('kategori')->select('id_kategori','nama')->get();

return view('transaksi.transaksi_edit',compact('kategori','Transaksi'));
}

public function view($id)
{
    $Transaksi = Transaksi::find($id);
    return view('view', ['transaksi' => $Transaksi]);
}


 public function delete($id)
{
    $Transaksi = Transaksi::where('id_transaksi', $id);
    $Transaksi->delete();
    return redirect('/transaksi');
}

 public function update(Request $request, $id)
    {

        $form_data = array(
            'jenis_trans'       =>   $request->jenis_trans,
            'nama'       =>   $request->nama_Transaksi,
            'nominal'         =>   $request->jk_Transaksi,
            'deskripsi'      =>   $request->kelas_Transaksi,
        );
  
        Transaksi::where('id_transaksi', $id)->update($form_data);

      return redirect('/Transaksi')->with(['success' => 'Data Berhasil di Update gaesss...']);
    }


}

