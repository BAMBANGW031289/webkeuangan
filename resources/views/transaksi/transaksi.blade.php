  @extends('layouts.master')

  @section('content')

  <section class="content-header">
        <h1>
          Master Transaksi
          <small>kamu mau mulai disini...</small>
        </h1>
      </section>


  <div class="container">
      <div class="row">
          <div class="col-lg-12">

            @if ($message = Session::get('success'))
              <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ $message }}</strong>
              </div>
            @endif

              <div class="panel panel-default">
                  <div class="panel-heading">CRUD Transaksi</div>
                   
                   <form method="post" action="/transaksi/store" enctype="multipart/form-data">

            {{ csrf_field() }}
                   
        


      <div class="panel-body">
          <div class="row">
            <div class="col-lg-6">
                    <!-- <div class="form-group row">
                          <label for="inputEmail3" style="text-align:right" class="col-lg-4 col-form-label">No Transaksi &nbsp; : </label>
                          <div class="col-lg-7">
                           <input type="text" name="kode_anggota" class="form-control" placeholder="No Transaksi ..">
                          </div>
                             @if($errors->has('kode_transaksi'))  
                                <div class="text-danger">
                                  {{ $errors->first('kode_transaksi')}}
                                    </div>
                                  @endif
                                </div> -->
                     <div class="form-group row">
                          <label for="inputEmail3" style="text-align:right" class="col-lg-4 col-form-label">Jenis Kategori &nbsp; : </label>
                          <div class="col-lg-7">
                           <select name="jenis_trans" class="form-control form-control-chosen" id="kd_project">
                            <option value="pemasukan">Pemasukan</option>
                              <option value="pengeluaran">Pengeluaran</option>
                            </select>
                          </div>
                             @if($errors->has('kode_transaksi'))  
                                <div class="text-danger">
                                  {{ $errors->first('kode_transaksi')}}
                                    </div>
                                  @endif
                                </div>

                      <div class="form-group row">
                          <label for="inputEmail3" style="text-align:right" class="col-lg-4 col-form-label">Kategori &nbsp; : </label>
                          <div class="col-lg-7">
                           <select name="nama" class="form-control form-control-chosen" id="kd_project">
                             <option value=""> --Pilih Kategori nama--</option>
                              @foreach ($kategori as $key)
                              <option value="{{ $key->nama}}">{{ $key->nama}}</option>
                                @endforeach
                            </select>
                          </div>
                             @if($errors->has('kode_transaksi'))  
                                <div class="text-danger">
                                  {{ $errors->first('kode_transaksi')}}
                                    </div>
                                  @endif
                                </div>          
                     

                      <div class="form-group row">
                          <label for="inputEmail3" style="text-align:right" class="col-lg-4 col-form-label">Nominal &nbsp; : </label>
                          <div class="col-lg-7">
                           <input type="text" id="nominal" name="nominal" class="form-control" placeholder="Nominal ..">
                          </div>
                             @if($errors->has('kelas'))  
                                <div class="text-danger">
                                  {{ $errors->first('kelas')}}
                                    </div>
                                  @endif
                                </div>
                      </div>
                    <div class="col-lg-6">
                       <div class="form-group row">
                          <label for="inputEmail3" style="text-align:right" class="col-lg-4 col-form-label">Deskripsi &nbsp; : </label>
                          <div class="col-lg-7">
                                  <textarea row="6" id="deskripsi" name="deskripsi" class="form-control"></textarea>
                          </div>
                             @if($errors->has('jk_anggota'))  
                                <div class="text-danger">
                                  {{ $errors->first('jk-anggota')}}
                                    </div>
                                  @endif
                                </div>

                    <div class="form-group row">
                          <label for="inputEmail3" style="text-align:right" class="col-lg-4 col-form-label">Saldo &nbsp; : </label>
                          <div class="col-lg-7">
                          <textarea row="4" id="deskripsi" name="saldo" class="form-control">{{ number_format($ttotal->total) }}</textarea>
                             </div>
                             @if($errors->has('jk_anggota'))  
                                <div class="text-danger">
                                  {{ $errors->first('jk-anggota')}}
                                    </div>
                                  @endif
                                </div>
                           
</div>
</div>

            <!-- <div class="form-group">
              <input type="submit" class="btn btn-success" value="Simpan">
            </div> -->
      
          </form>
                    <button type="submit" class="btn btn-success">Save</button>
                      <br/>
                      <br/>
                      <table id="tb_Transaksi" class="table table-bordered table-hover table-striped display">
                          <thead>
                              <tr>
                                  <th style="vertical-align: middle;text-align: center;">No</th>
                                  <th style="vertical-align: middle;text-align: center;">Jenis kategori</th>
                                  <th style="vertical-align: middle;text-align: center;">Nama</th>
                                  <th style="vertical-align: middle;text-align: center;">Nominal</th>
                                  <th style="vertical-align: middle;text-align: center;">Deskripsi</th>
                                  <th style="vertical-align: middle;text-align: center;">action</th>
                              </tr>
                          </thead>
                          <tbody>
                              @foreach($transaksi as $angka => $p)
                              <tr>
                                  <td style="text-align:center">{{ ++$angka }}</td>
                                  <td>{{ $p->jenis_trans }}</td>
                                  <td>{{ $p->nama }}</td>
                                  <td>{{ number_format($p->nominal) }}</td>
                                  <td>{{ $p->deskripsi }}</td>
                                  <td style="text-align:center" width="20%" >
                                      <a href="/transaksi/edit/{{ $p->id_transaksi }}" class="btn btn-warning">Update</a>
                                      <a href="/transaksi/hapus/{{ $p->id_transaksi }}" class="btn btn-danger">Delete</a>
                                  </td>
                              </tr>
                              @endforeach
                          </tbody>
                      </table>
                      
                 <table style="margin-bottom:3px;text-align:left;margin-top: 5px; margin-right: auto; float:right">
                   <tr  bgcolor="skyblue">  <td width="140"><b>Jumlah Transaksi </b></td><td width="10">:</td><td width="10"><b> <?php echo count($transaksi); ?></b></td></tr>
                   </tr>
                    </table>
                      <a style="text-align:right" href="/home" class="btn btn-primary">Back</a>
                  </div>
              </div>
            </div>
        </div>
      </div>
      

        @endsection
  <!-- </html> -->
