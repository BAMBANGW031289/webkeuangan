@extends('layouts.master')

@section('content')
<section class="content-header">
      <h1>
        Master Peminjaman
        <small>kamu mau mulai disini...</small>
      </h1>
    </section>

<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">CRUD Peminjaman</div>

        <div class="panel-body">
          <a href="/peminjaman" class="btn btn-primary">Kembali</a>
          <br/>
          <br/>
          <form method="post" action="/peminjaman/store" enctype="multipart/form-data">

            {{ csrf_field() }}

             <div class="form-group">
              <label>Kode Peminjaman</label>
              <input type="text" name="kode_peminjaman" class="form-control" placeholder="Kode Peminjaman ..">

              @if($errors->has('kode_peminjaman'))  
              <div class="text-danger">
                {{ $errors->first('kode_peminjaman')}}
              </div>
              @endif
            </div>

             <div class="form-group">
              <label>Nama</label>
              <input type="text" name="nama_peminjaman" class="form-control" placeholder="Nama ..">

              @if($errors->has('nama_peminjaman'))
              <div class="text-danger">
                {{ $errors->first('nama_peminjaman')}}
              </div>
              @endif
            </div>

              <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <select name="jk_peminjaman" class="form-control" placeholder="Jenis Kelamin ..">
                              <option value="Laki-laki">Laki-laki</option>
                              <option value="Perempuan">Perempuan</option>
                            </select>
                            @if($errors->has('jk_peminjaman'))
                                <div class="text-danger">
                                    {{ $errors->first('jk_peminjaman')}}
                                </div>
                            @endif

                 </div>

             <div class="form-group">
              <label>Kelas/*dikosongkan jika non kelas</label>
              <input type="text" name="kelas_peminjaman" class="form-control" placeholder="Kelas Peminjaman ..">

              @if($errors->has('kelas_peminjaman'))
              <div class="text-danger">
                {{ $errors->first('kelas_peminjaman')}}
              </div>
              @endif
            </div>


            <div class="form-group">
              <label>No telp</label>
              <input type="text" name="notelp_peminjaman" class="form-control" placeholder="Nomor Telpon ..">

              @if($errors->has('notelp_peminjaman'))
              <div class="text-danger">
                {{ $errors->first('notelp_amggota')}}
              </div>
              @endif
            </div>

             <div class="form-group">
              <label>alamat</label>
              <input type="text" name="alamat_peminjaman" class="form-control" placeholder="alamat ..">

              @if($errors->has('alamat_peminjaman'))
              <div class="text-danger">
                {{ $errors->first('alamat_peminjaman')}}
              </div>
              @endif
            </div>

            <!-- <div class="form-group">
              <label>Image Peminjaman</label>
              <input type="file" name="image" class="form-control" placeholder="images ..">

              @if($errors->has('image'))
              <div class="text-danger">
                {{ $errors->first('image')}}
              </div>
              @endif
            </div> -->

<!-- 
            <div class="form-group">
                <label>Kategori Peminjaman</label>
                <select name="kategori" class="form-control" placeholder="Nama category ..">
                  @foreach ($kategori as $key)
                       <option value="{{ $key->id_kategori}}"> {{$key->nama}}</option>
                  @endforeach
                </select>


                @if($errors->has('kategori'))
                    <div class="text-danger">
                        {{ $errors->first('kategori')}}
                    </div>
                @endif

            </div> -->

            <div class="form-group">
              <input type="submit" class="btn btn-success" value="Simpan">
            </div>

          </form>

        </div>
      </div>
    </div>

  </div>
</div>
@endsection
