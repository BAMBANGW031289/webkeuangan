@extends('layouts.master')

@section('content')

<section class="content-header">
      <h1>
        Master Anggota
        <small>kamu mau mulai disini...</small>
      </h1>
    </section>

<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Edit Data Anggota</div>

        <div class="panel-body">
          <a href="/anggota" class="btn btn-primary">Kembali</a>
          <br/>
          <br/>


                    <form method="post" action="/anggota/update/{{ $anggota->id_anggota }}" enctype="multipart/form-data">

                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                                     <div class="form-group">
              <label>Kode Anggota</label>
              <input type="text" name="kode_anggota" class="form-control" value="{{ $anggota->kode_anggota }}" placeholder="Kode Anggota ..">

              @if($errors->has('kode_anggota'))
              <div class="text-danger">
                {{ $errors->first('kode_anggota')}}
              </div>
              @endif
            </div>

             <div class="form-group">
              <label>Nama</label>
              <input type="text" name="nama_anggota" class="form-control" value="{{ $anggota->nama_anggota }}" placeholder="nama Anggota ..">

              @if($errors->has('nama_anggota'))
              <div class="text-danger">
                {{ $errors->first('nama_anggota')}}
              </div>
              @endif
            </div>

             <div class="form-group">
              <label>Jenis Kelamin</label>
              <input type="text" name="jk_anggota" class="form-control" value="{{ $anggota->jk_anggota }}" placeholder="Jenis Kelamin Anggota ..">

              @if($errors->has('jk_anggota'))
              <div class="text-danger">
                {{ $errors->first('jk_anggota')}}
              </div>
              @endif
            </div>

             <div class="form-group">
              <label>Kelas</label>
              <input type="text" name="kelas_anggota" class="form-control" value="{{ $anggota->kelas_anggota }}" placeholder="kelas Anggota ..">
              @if($errors->has('kelas_anggota'))
              <div class="text-danger">
                {{ $errors->first('kelas_anggota')}}
              </div>
              @endif
            </div>


            <div class="form-group">
              <label>No Telp</label>
              <input type="text" name="notelp_anggota" class="form-control" value="{{ $anggota->notelp_anggota }}" placeholder="No Telp Anggota ..">

              @if($errors->has('notelp_anggota'))
              <div class="text-danger">
                {{ $errors->first('notelp_anggota')}}
              </div>
              @endif
            </div>

             <div class="form-group">
              <label>Alamat</label>
              <input type="text" name="alamat_anggota" class="form-control" value="{{ $anggota->alamat_anggota }}" placeholder="alamat anggota">

              @if($errors->has('alamat_anggota'))
              <div class="text-danger">
                {{ $errors->first('alamat_anggota')}}
              </div>
              @endif
            </div>

                        <!-- <div class="form-group">
                            <label>Kategori</label>
                                <select name="kategori" class="form-control" placeholder="Nama category ..">
                                @foreach($kategori as $key)
                                    <option value="{{ $key->id_kategori }}" {{ $key->id_kategori == $anggota->kategori ? 'selected' : '' }}>{{ $key->nama }}</option>
                                @endforeach
                                </select>
                        </div> -->

<!-- 
                         <div class="form-group">
                            <label>Image Product</label>
                        <input type="file" name="image" />
                         <img src="{{ asset('img/'. $anggota->image) }}" class="img-thumbnail" width="100" />
                        <input type="hidden" name="hidden_image" value="{{ $anggota->image }}" />

                            @if($errors->has('image'))
                            <div class="text-danger">
                                {{ $errors->first('image')}}
                            </div>
                            @endif
                            </div> -->

                     
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>

                    </form>

                  </div>
                </div>
                </div>
                </div>
                </div>

                @endsection

