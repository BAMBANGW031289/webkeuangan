@extends('layouts.master')

@section('content')
<section class="content-header">
      <h1>
        Master Product
        <small>kamu mau mulai disini...</small>
      </h1>
    </section>

<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">CRUD Kategori Tambah</div>

        <div class="panel-body">
          <a href="/kategori" class="btn btn-primary">Kembali</a>
          <br/>
          <br/>

                    <form method="post" action="/kategori/store">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="nama" class="form-control" placeholder="Nama kategori ..">

                            @if($errors->has('kategori'))
                                <div class="text-danger">
                                    {{ $errors->first('kategori')}}
                                </div>
                            @endif

                        </div>



                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>

                    </form>
                  </div>
                </div>
                </div>
                </div>
                </div>

                @endsection
