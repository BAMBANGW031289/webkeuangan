@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">CRUD Kategori Edit</div>

        <div class="panel-body">
          <a href="/kategori" class="btn btn-primary">Kembali</a>
          <br/>
          <br/>
                    <form method="post" action="/kategori/update/{{ $kategori->id_kategori }}">

                        {{ csrf_field() }}
                        {{ method_field('PUT') }}



                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="jenis" class="form-control" placeholder="Nama kategori .." value=" {{ $kategori->jenis }}">

                            @if($errors->has('kategori'))
                                <div class="text-danger">
                                    {{ $errors->first('kategori')}}
                                </div>
                            @endif

                        </div>
                           <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="nama" class="form-control" placeholder="Nama kategori .." value=" {{ $kategori->nama }}">

                            @if($errors->has('kategori'))
                                <div class="text-danger">
                                    {{ $errors->first('kategori')}}
                                </div>
                            @endif

                        </div>
                           <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="deskripsi" class="form-control" placeholder="Nama kategori .." value=" {{ $kategori->deskripsi }}">

                            @if($errors->has('kategori'))
                                <div class="text-danger">
                                    {{ $errors->first('kategori')}}
                                </div>
                            @endif

                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>

                    </form>
                  </div>
                </div>
                </div>
                </div>
                </div>

                @endsection
