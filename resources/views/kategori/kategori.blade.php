@extends('layouts.master')

@section('content')

<section class="content-header">
      <h1>
        Master Kategori
        <small>kamu mau mulai disini...</small>
      </h1>
    </section>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

          @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
          @endif

         @if ($message = Session::get('danger'))
            <div class="alert alert-danger alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
          @endif

            <div class="panel panel-default">
                <div class="panel-heading">CRUD Kategori</div>

                <div class="panel-body">
                  <a class="btn btn-success" data-toggle="modal" data-target="#modal_kategori">Input Kategori Baru</a>
                    <br/>
                    <br/>
                    <table id="tb_datatable" class="table table-bordered table-hover table-striped display">
                        <thead>
                            <tr>
                                <th>Jenis</th>
                                <th>Nama Kategori</th>
                                <th>Deskripsi</th>
                                <th>action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($kategori as $p)
                            <tr>
                              <td>{{ $p->jenis }}</td>
                                <td>{{ $p->nama }}</td>
                                 <td>{{ $p->deskripsi }}</td>
                                <td>
                                    <!-- <a data-toggle="modal" 
                            data-target="#myModal" class="btn btn-warning">Edit</a> -->
                                    <a href="/kategori/edit/{{ $p->id_kategori }}" class="btn btn-warning">Update</a>
                                    <a href="/kategori/hapus/{{ $p->id_kategori }}" class="btn btn-danger">Delete</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <a style="text-align:right" href="/home" class="btn btn-primary">Back</a>
                  </div>
              </div>
            </div>
        </div>
      </div>
         <!-- Modal -->
<div class="modal fade" id="modal_kategori" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div style="background:#52AB93; color:white" class="modal-header">
        <h5  style="color:white" class="modal-title" id="exampleModalLabel">Tambah Kategori :</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form action="{{ url('/kategori/store') }}" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
          <div class="form-group">
        <div class="form-group">
        <label for="exampleFormControlSelect1"> Jenis Kategori : </label>
        <select name="jenis" class="form-control form-control-chosen" id="kd_project">
              <option value="pemasukan">Pemasukan</option>
                <option value="pengeluaran">Pengeluaran</option>
              </select>
         </div>
        <div class="form-group">
          <label for="exampleFormControlSelect1">Nama Kategori : </label>
              <input class="form-control" type="text" name="nama" placeholder="isikan Nama Kategori" id="kategori_name">        
          </div>
              <div class="form-group">
          <label for="exampleFormControlSelect1">Deskripsi : </label>
              <textarea row="4" class="form-control" name="deskripsi" placeholder="isikan Deskripsi" id="Deskripsi"></textarea>        
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="submit" id="simpan" class="btn btn-primary">Simpan</button>
      </div>
    </div>
    </form>
  </div>
</div>
</div>
        @endsection
