<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix'=>'dropdown'], function () {
    Route::get('/kategori', function(){
    return view('kategori');
  })->name('kategori');
});

Route::group(['prefix'=>'dropdown'], function () {
    Route::get('/buku', function(){
    return view('buku');
  })->name('buku');
});



Route::get('/kategori', 'KategoriController@index');
Route::get('/kategori/tambah', 'KategoriController@tambah');
Route::post('/kategori/store', 'KategoriController@store');
Route::get('/kategori/edit/{id_kategori}', 'KategoriController@edit');
Route::put('/kategori/update/{id_kategori}', 'KategoriController@update');
Route::get('/kategori/hapus/{id_kategori}', 'KategoriController@delete');

Route::get('/buku', 'BukuController@index');
Route::get('/buku/tambah', 'BukuController@tambah');
Route::post('/buku/store', 'BukuController@store');
Route::get('/buku/edit/{id}', 'BukuController@edit');
Route::get('/buku/view/{id}', 'BukuController@view');
Route::put('/buku/update/{id}', 'BukuController@update');
Route::get('/buku/hapus/{id}', 'BukuController@delete');

Route::get('/anggota', 'AnggotaController@index');
Route::get('/anggota/tambah', 'AnggotaController@tambah');
Route::post('/anggota/store', 'AnggotaController@store');
Route::get('/anggota/edit/{id}', 'AnggotaController@edit');
Route::get('/anggota/view/{id}', 'AnggotaController@view');
Route::put('/anggota/update/{id}', 'AnggotaController@update');
Route::get('/anggota/hapus/{id}', 'AnggotaController@delete');

Route::get('/user', 'UserController@index');
Route::get('/user/tambah', 'UserController@tambah');
Route::post('/user/store', 'UserController@store');
Route::get('/user/edit/{id}', 'UserController@edit');
Route::get('/user/view/{id}', 'UserController@view');
Route::put('/user/update/{id}', 'UserController@update');
Route::get('/user/hapus/{id}', 'UserController@delete');

Route::get('/transaksi', 'TransaksiController@index');
Route::get('/transaksi/tambah', 'TransaksiController@tambah');
Route::post('/transaksi/store', 'TransaksiController@store');
Route::get('/transaksi/edit/{id}', 'TransaksiController@edit');
Route::get('/transaksi/view/{id}', 'TransaksiController@view');
Route::put('/transaksi/update/{id}', 'TransaksiController@update');
Route::get('/transaksi/hapus/{id}', 'TransaksiController@delete');
Route::get('/cari', 'TransaksiController@loadData');
Route::get('searchajax',array('as'=>'searchajax','uses'=>'TransaksiController@autoComplete'));
Route::get('/transaksi/getInfo/{id}', 'TransaksiController@getInfo');



Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
